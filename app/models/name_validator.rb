class  NameValidator
  include ActiveModel::Model
  include ActiveModel::Serialization
  attr_accessor :name
  validates :name, presence: true
  validates_with SatanValidator

  def valid
    valid?
  end

  # needed for serialization
  def attributes
   {'name' => nil, 'valid' => nil}
  end
end
