class PersonDetailsController < ApplicationController
  def get_person
  end

  def show_person
    @nv = NameValidator.new
    @nv.name = params[:name]
    @is_it_valid = @nv.valid?

    respond_to do |format|
        format.html {}
        format.json { render json: @nv.serializable_hash }
    end
  end
end
