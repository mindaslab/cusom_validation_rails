Rails.application.routes.draw do
  get 'person_details/get_person'
  post 'person_details/show_person'
  resources :people
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
